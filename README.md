# Bozarre-pi

Do you want to send me an audio file and play it remotely in my apartment ? Well, now you can !

A nodejs server running on a raspberry pi connected to a speaker allows anyone to send wav files and play them on the server.

![Cybernetic Technician](Cybernetic Technician.mp4)
![Good Afternoon](Good Afternoon.mp4)

Try here : http://82.65.41.82:8080 (works only when my raspberry pi is plugged)

You want to setup your own Bozarre-pi ?

# Installation

## Core updates

`sudo apt-get update`

`sudo apt-get dist-upgrade`

## Install node

`sudo curl -sL https://deb.nodesource.com/setup_10.x | bash -`

`sudo apt-get install -y nodejs`

## Node server folder

`mkdir nodeserver`

`cd nodeserver`

You can then copy the files in it.

## Install dependencies

`sudo apt install npm`

`sudo npm i socket.io`

`sudo npm i node-static`

`sudo npm i formidable`

`sudo npm i aplay`

## Start the server

`node webserver.js`

VOILÀ !


## Setting up node boot

https://medium.com/@andrew.nease.code/set-up-a-self-booting-node-js-eb56ebd05549


