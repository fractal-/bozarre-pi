//Feel free to do whatever you want with it
//
// Originally assembled by
// BOZARRE aka Jeremie Gabolde
//
// https://gitlab.com/fractal-/bozarre-pi


//const http = require('http').createServer(handler); //require http server, and create server with function handler()
const express = require('express'); //require express to handle multi rooting
var app = express();
const http = require('http').createServer(app);
const fs = require('fs'); //require filesystem module
const io = require('socket.io')(http); //require socket.io module and pass the http object (server)

//handle rooting

//handle static files serving
var static = require('node-static');
var file = new static.Server('public', { cache: 3600 });

//handles upload form
var formidable = require('formidable');
//upload location
const uploadsFolder = 'public/upload/';

//handle pi sound playing
var sound = require('aplay');
var music = new sound();
var isPlaying = false;
var currentSoundName = "";
var timerStopSound;

var timerTimeout;

//handle external python script exec
const {PythonShell} = require('python-shell');

let options = {
    mode: 'text',
    pythonPath: '/usr/bin/python3',
    pythonOptions: ['-u'], // get print results in real-time
    scriptPath: '/home/pi/nodeserver',
    args: ['hover']
};

//Start the server
http.listen(80); //listen to port 8080

require('events').EventEmitter.defaultMaxListeners = 64;

app.get('/', function (req, res) {
  res.sendFile('./public/index.html', { root: __dirname });
});

app.get('/timer', function (req, res) {
  res.sendFile('./public/timer.html', { root: __dirname });
})

function handler (req, res) { //create server

  //file upload
  if (req.url == '/' && req.method.toLowerCase() == 'post')
  {
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files)
    {
		if (files.filetoupload.type == 'audio/wav')
		{
			var oldpath = files.filetoupload.path;
			console.log(oldpath);
			if (oldpath)
			{
				var newpath = uploadsFolder + files.filetoupload.name;
				fs.rename(oldpath, newpath, function (err)
				{
				if (err) throw err;
				console.log('File uploaded and moved!');
				});
			}
		}
    });
  }

  //public files serving
  req.addListener('end', function ()
  {
	// Serve files!
	file.serve(req, res);
  }).resume();

}


io.sockets.on('connection', function (socket)
{// WebSocket Connection

	//List Audio Files
	fs.readdir(uploadsFolder, (err, files) => {
		io.sockets.emit('setAudioFiles', files);
	});

	socket.on('playSound', function(data) { //get button
		if (!isPlaying)
		{
			music.play(uploadsFolder + data);
			isPlaying = true;
			currentSoundName = data;
			console.log('Playing the Sound ' + currentSoundName);
			io.sockets.emit('log', "Playing the Sound "  + currentSoundName);
			io.sockets.emit('playSound', currentSoundName);
			if (timerStopSound) clearTimeout(timerStopSound);
			timerStopSound = setTimeout(SoundStop, 15000);
			//spawn('python3', ['arduinoHelicopter.py', 'hover']);

		}
		else
		{
			console.log('Already Playing ' + currentSoundName);
			io.sockets.emit('log', "Already Playing " + currentSoundName);
		}
	});

 	socket.on('getPlayerInfo', function(data) {

		if (!isPlaying)
		{
			io.sockets.emit('log', "Player Available");
		}
		else
		{
			io.sockets.emit('log', "Already Playing " + currentSoundName);
		}
	});

  socket.on('sendCode', function(data) {
    io.sockets.emit('log', "Sending Code " + data);

    options.args = [data];

    PythonShell.run('arduinoHelicopter.py', options);

  });

	music.on('complete', function () {
		if (isPlaying == true)
		{
			isPlaying = false;
			console.log('Sound finished');
			socket.emit('log', "Sound finished");
			io.sockets.emit('log', "Player Available");
		}
	});

	function SoundStop()
	{
		music.stop();
		isPlaying = false;
		console.log('Sound finished');
		socket.emit('log', "Sound finished");
		io.sockets.emit('log', "Player Available");
	}

  socket.on('startTimer', function(data) {
    io.sockets.emit('log', "Starting timer " + data);
    clearTimeout(clamp(timerTimeout, 0, 1440*1000));
    timerTimeout = setTimeout(endTimer, data);
  });

});

function endTimer(arg) {
  io.sockets.emit('log', "Timer ended ");

  options.args = "hover";

  PythonShell.run('arduinoHelicopter.py', options, function (err, results) {
    if (err) throw err;
    // results is an array consisting of messages collected during execution
    console.log('results: %j', results);
  });

  io.sockets.emit('TimerEnded');
}

function clamp(val, min, max)
{
  return val > max ? max : val < min ? min : val;
}

console.log("Server created Successfully")
